import select
import socket


# Add address of your ESP32
ALLOWED_ADDRESSES = ['192.168.111.58']

# Set port to open
OPEN_PORT = 1060


def all_events_forever(poll_object):
    while True:
        for f_d, event in poll_object.poll():
            yield f_d, event

def serve(listener):
    sockets = {listener.fileno(): listener}
    addresses = {}
    bytes_received = {}
    poll_object = select.poll()
    poll_object.register(listener, select.POLLIN)

    for f_d, event in all_events_forever(poll_object):
        sock = sockets[f_d]

        if event & (select.POLLHUP | select.POLLERR | select.POLLNVAL):
            address = addresses.pop(sock)
            poll_object.unregister(f_d)
            del sockets[f_d]

        elif sock is listener:
            sock, address = sock.accept()

            if address[0] in ALLOWED_ADDRESSES:
                sock.setblocking(False)
                sockets[sock.fileno()] = sock
                addresses[sock] = address
                poll_object.register(sock, select.POLLIN)
            else:
                print(f"Address {address[0]} not in `ALLOWED_ADDRESSES` of the server")

        elif event & select.POLLIN:
            more_data = sock.recv(4096)

            if not more_data:
                sock.close()

                for _, value in bytes_received.items():
                    value = str(value)
                    dataIndex = value.find("data=") + 5
                    data = value[dataIndex:-1]
                    print(data) # Do whatever you want with recieved data here
                    bytes_received.clear()
                    break

                continue

            data = bytes_received.pop(sock, b'') + more_data
            bytes_received[sock] = data




def create_srv_socket(address=("0.0.0.0", OPEN_PORT)):
    listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listener.bind(address)
    listener.listen(64)
    return listener


if __name__ == '__main__':
    LISTENER = create_srv_socket()
    serve(LISTENER)
