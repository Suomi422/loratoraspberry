#include <SPI.h>
#include <LoRa.h>
#include <HTTPClient.h>

#include "WiFi.h"



#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    923E6

// Wifi Settings
#define WIFI_NETWORK "mynetwork"
#define WIFI_PASSWORD "mypassword"
#define URL "http://192.168.1.5:1060" // sample


void connectToWiFi() {
    Serial.println(" ");
    Serial.print("[ INF ] Connecting to WiFi");
    Serial.print(" ");
    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFI_NETWORK, WIFI_PASSWORD);

    unsigned long startAttemptTime = millis();

    while(WiFi.status() != WL_CONNECTED && millis() - startAttemptTime < 20000) {
        Serial.print(".");
        delay(500);
    }

    Serial.println("");

    if (WiFi.status() != WL_CONNECTED) {
        Serial.println("[ ERR ] Failed connect to WiFi!");
        delay(5000);

    }
    else {
        String connected = "";
        String ip_add_ = WiFi.localIP().toString();
        connected += "[ SUCC ] Connected to WiFi:";
        connected += String(WIFI_NETWORK);
        connected += " - with IP address ";
        connected += ip_add_;
        Serial.println(connected);
    }
}



void parseLora(int packetSize) {
    String packet = "";
    for (int i = 0; i < packetSize; i++) {
        packet += (char) LoRa.read();
    }

    String rssi = "";
    rssi = "[ INF ] Radio signal strength = " + String(LoRa.packetRssi(), DEC);
    Serial.print(rssi);
    Serial.println("\n");
    Serial.print("[ INF ] Recieved data:");
    Serial.print(packet);

    WiFiClient Wclient;
    HTTPClient Hclient;
    Hclient.begin(Wclient, URL);
    Hclient.addHeader("Content-Type", "application/x-www-form-urlencoded");

    String httpRequestData = "data=" + packet;           
    int httpResponseCode = Hclient.POST(httpRequestData);

    Serial.println("[ INF ] Sending data to the address ...");
    Serial.print("[ INF ] HTTP Response code: ");
    Serial.println(httpResponseCode);

    Hclient.end();
}



void setup() {
    Serial.begin(115200);
    while (!Serial) {
        delay(200);
    }
    connectToWiFi();
    Serial.println("[ INF ] LoRa Receiver Device");
    SPI.begin(SCK, MISO, MOSI, SS);
    LoRa.setPins(SS, RST, DI0);  
    int tries = 0;
    while (!LoRa.begin(BAND) && tries < 10) {
        Serial.println("[ ERR ] Starting LoRa failed!");
        tries++;
    }

    if (tries != 10) {
        Serial.println("[ SUCC ] init ...ok");
        LoRa.receive();
        delay(1000);
    }
    else {
        Serial.println("[ ERR ] Not able to initialize LoRa");
        Serial.println("[ INF ] Restarting device after 30 seconds");
        delay(30000);
        ESP.restart();
    }
}



void loop() {
    if (WiFi.status() == WL_CONNECTED) {
        int packetSize = LoRa.parsePacket();
        if (packetSize) {
            parseLora(packetSize);
        }
        delay(10);
    }
    else {
        Serial.println("[ ERR ] Wifi is not connected. Restarting device after 30 seconds.");
        delay(30000);
        ESP.restart();
    }
}
