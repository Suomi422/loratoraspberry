# LoRaToRaspberry

Project made for parsing data from LoRa devices (especially LilyGo TTGO T-Beam 1.1) to more 'classical' device like Raspberry Pi, or any Unix system capable running Python3.

## Enviroment

For this project you will need:

● 2 x LilyGo TTGO T-Beam 1.1 (or any esp32 with LoRa device <- code needs to be edited for specifical device).

● 1x RaspberryPi or any UNIX system capable runing Python3.

● Wi-Fi network (can be created separately on one of the ESP32 devices like AP, but in this version we will using proper Wi-Fi Router access).

● Arduino IDE for uploading source code to ESP32 devices.




## Python Server

Server for receiving data can be run simple by:
```
python3 server.py
```


## Whole Flow

LoRa_sender_DeepSleep.ino ===> (Sending example data over LoRa and sleeping) ===> LoRa_receiver_parse.ino ===> (Receiving data and sending it over TCP to server.py) ===> server.py (receiving data and printing it).


## !! IMPORTANT !!

Please be sure to change variables below for your aplication:

● `ALLOWED_ADDRESSES = ['192.168.111.58']` in server.py (For allowing receiving data from ESP32)

● `OPEN_PORT = 1060` in server.py

● `#define WIFI_NETWORK "mynetwork"` in LoRa_reciever_parse.ino

● `#define WIFI_PASSWORD "mypassword"` in LoRa_reciever_parse.ino

● `#define URL "http://192.168.1.5:1060"` in LoRa_reciever_parse.ino (For sending data over TCP)

● `#define BAND  923E6` in LoRa_sender_DeepSleep.ino (on the region you live in for LoRa transmission)

● `#define TIME_TO_SLEEP  30` in LoRa_sender_DeepSleep.ino (For time to Deep Sleep -> seconds)

● `#define TIME_TO_LIVE   10` in LoRa_sender_DeepSleep.ino (For minimal time to be awaken -> seconds)
