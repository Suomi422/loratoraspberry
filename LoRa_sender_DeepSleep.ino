#include <SPI.h>
#include <LoRa.h>



#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISnO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND  923E6

// For deep sleep and timers
#define uS_TO_S_FACTOR 1000000
#define mS_TO_S_FACTOR 1000
#define TIME_TO_SLEEP  30 // how many seconds should sleep?
#define TIME_TO_LIVE   10 // how many seconds should be woke up?


RTC_DATA_ATTR int bootCount = 0;



void setup() {
    unsigned long startTime = millis();

    Serial.begin(115200);
    ++bootCount;
    delay(1000);
    Serial.println("[ INF ] LoRa Sender Device");

    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    Serial.println("[ INF ] Setup device to sleep for every " + String(TIME_TO_SLEEP) + " Seconds");

    SPI.begin(SCK,MISO,MOSI,SS);
    LoRa.setPins(SS,RST,DI0);

    int tries = 0;
    while (!LoRa.begin(BAND) && tries < 10) {
        Serial.println("[ ERR ] Starting LoRa failed!");
        tries++;
    }

    if (tries != 10) {
        Serial.println("[ SUCC ] init ...ok");
        delay(1000);

        LoRa.beginPacket();
        LoRa.print("Rebooted ");
        LoRa.print(bootCount);
        LoRa.print(" times");
        LoRa.endPacket();
    }

    else {
        Serial.println("[ ERR ] Not able to initialize LoRa");
    }

    unsigned int timeToLiveMilis = TIME_TO_LIVE * mS_TO_S_FACTOR;
    unsigned long endTime = millis();
    unsigned int diff = endTime - startTime;
    if (diff < timeToLiveMilis) {
        unsigned int sleep_time = timeToLiveMilis - diff;
        delay(sleep_time);
    }

    Serial.println("[ INF ] Going to deep sleep now");
    Serial.flush(); 
    esp_deep_sleep_start();
}

void loop() {}